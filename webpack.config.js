let path = require('path');

module.exports = {
    entry: './vue/vue-instance.js',
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: 'bundle.js'
    },
    module:{
        rules: [
            {
                test:/\.vue$/,
                loader: 'vue-loader',
            },
        ]
    }
};
export default {

    heroes: [
        {
            id: 1,
            name: 'Doomfist',
            imageUrl: 'https://d1u1mce87gyfbn.cloudfront.net/hero/doomfist/hero-select-portrait.png'
        },
        {
            id: 2,
            name: 'Genji',
            imageUrl: 'https://d1u1mce87gyfbn.cloudfront.net/hero/genji/hero-select-portrait.png'
        },
        {
            id: 3,
            name: 'McCree',
            imageUrl: 'https://d1u1mce87gyfbn.cloudfront.net/hero/mccree/hero-select-portrait.png'
        },
        {
            id: 4,
            name: 'Pharah',
            imageUrl: 'https://d1u1mce87gyfbn.cloudfront.net/hero/pharah/hero-select-portrait.png'
        },
        {
            id: 5,
            name: 'Reaper',
            imageUrl: 'https://d1u1mce87gyfbn.cloudfront.net/hero/reaper/hero-select-portrait.png'
        },
        {
            id: 6,
            name: 'Soldier: 76',
            imageUrl: 'https://d1u1mce87gyfbn.cloudfront.net/hero/soldier-76/hero-select-portrait.png'
        },
        {
            id: 7,
            name: 'Sombra',
            imageUrl: 'https://d1u1mce87gyfbn.cloudfront.net/hero/sombra/hero-select-portrait.png'
        },
        {
            id: 8,
            name: 'Tracer',
            imageUrl: 'https://d1u1mce87gyfbn.cloudfront.net/hero/tracer/hero-select-portrait.png'
        },
        {
            id: 9,
            name: 'Bastion',
            imageUrl: 'https://d1u1mce87gyfbn.cloudfront.net/hero/bastion/hero-select-portrait.png'
        },
        {
            id: 10,
            name: 'Hanzo',
            imageUrl: 'https://d1u1mce87gyfbn.cloudfront.net/hero/hanzo/hero-select-portrait.png'
        },
        {
            id: 11,
            name: 'Junkrat',
            imageUrl: 'https://d1u1mce87gyfbn.cloudfront.net/hero/junkrat/hero-select-portrait.png'
        },
        {
            id: 12,
            name: 'Mei',
            imageUrl: 'https://d1u1mce87gyfbn.cloudfront.net/hero/mei/hero-select-portrait.png'
        },
        {
            id: 13,
            name: 'Torbjörn',
            imageUrl: 'https://d1u1mce87gyfbn.cloudfront.net/hero/torbjorn/hero-select-portrait.png'
        },
        {
            id: 14,
            name: 'Widowmaker',
            imageUrl: 'https://d1u1mce87gyfbn.cloudfront.net/hero/widowmaker/hero-select-portrait.png'
        },
        {
            id: 15,
            name: 'D.Va',
            imageUrl: 'https://d1u1mce87gyfbn.cloudfront.net/hero/dva/hero-select-portrait.png'
        },
        {
            id: 16,
            name: 'Orisa',
            imageUrl: 'https://d1u1mce87gyfbn.cloudfront.net/hero/orisa/hero-select-portrait.png'
        },
        {
            id: 17,
            name: 'Reinhardt',
            imageUrl: 'https://d1u1mce87gyfbn.cloudfront.net/hero/reinhardt/hero-select-portrait.png'
        },
        {
            id: 18,
            name: 'Roadhog',
            imageUrl: 'https://d1u1mce87gyfbn.cloudfront.net/hero/roadhog/hero-select-portrait.png'
        },
        {
            id: 19,
            name: 'Winston',
            imageUrl: 'https://d1u1mce87gyfbn.cloudfront.net/hero/winston/hero-select-portrait.png'
        },
        {
            id: 20,
            name: 'Zarya',
            imageUrl: 'https://d1u1mce87gyfbn.cloudfront.net/hero/zarya/hero-select-portrait.png'
        },
        {
            id: 21,
            name: 'Ana',
            imageUrl: 'https://d1u1mce87gyfbn.cloudfront.net/hero/ana/hero-select-portrait.png'
        },
        {
            id: 22,
            name: 'Brigitte',
            imageUrl: 'https://d1u1mce87gyfbn.cloudfront.net/hero/brigitte/hero-select-portrait.png'
        },
        {
            id: 23,
            name: 'Lúcio',
            imageUrl: 'https://d1u1mce87gyfbn.cloudfront.net/hero/lucio/hero-select-portrait.png'
        },
        {
            id: 24,
            name: 'Mercy',
            imageUrl: 'https://d1u1mce87gyfbn.cloudfront.net/hero/mercy/hero-select-portrait.png'
        },
        {
            id: 25,
            name: 'Moira',
            imageUrl: 'https://d1u1mce87gyfbn.cloudfront.net/hero/moira/hero-select-portrait.png'
        },
        {
            id: 26,
            name: 'Symmetra',
            imageUrl: 'https://d1u1mce87gyfbn.cloudfront.net/hero/symmetra/hero-select-portrait.png'
        },
        {
            id: 27,
            name: 'Zenyatta',
            imageUrl: 'https://d1u1mce87gyfbn.cloudfront.net/hero/zenyatta/hero-select-portrait.png'
        }
    ],

    getHeroById(heroId) {
        return this.heroes.find(
            function (obj) {
                return obj.id === Number(heroId);
            }
        )
    },

    getHeroByIndex(index) {
        return this.heroes[index];
    },

    getHeroesByName(string) {
        let results = [];
        this.heroes.forEach(function (obj) {
                if (obj.name.includes(string.charAt(0).toUpperCase() + string.slice(1))) {
                    results.push(obj);
                }
            }
        );
        return results
    },

    addHero(name, imageUrl) {
        if (imageUrl === ''){
            imageUrl = "http://s3.amazonaws.com/37assets/svn/765-default-avatar.png"
        }
        this.heroes.push({id: this.heroes.length+1, name: name, imageUrl: imageUrl})
    },

    removeHero(heroId) {
        let Id;
        this.heroes.find(
            function (obj) {
                if (obj.id === Number(heroId)) {
                    Id = obj;
                    return Id
                }
            },
        );
        let name= Id.name;
        this.heroes.splice(this.heroes.indexOf(Id), 1);
        return name
    }
}
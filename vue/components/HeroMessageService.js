export default {

    log: [],

    addLogEntry(message, fileName){
      this.log.push(
          {
              timestamp: this.getDateTineFromUnix(),
              message: message,
              from: fileName
          }
      )
    },

    clearLog(){
        this.log = []
    },

    getDateTineFromUnix(){
        return new Date().getHours() + ':'
               + new Date().getMinutes() + ':'
               + new Date().getSeconds()
    }
}
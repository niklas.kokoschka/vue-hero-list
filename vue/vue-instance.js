import VueRouter from 'vue-router'

require('./vue-components');

import HeroDetail from './components/HeroDetail.vue';
import HeroDashborad from './components/HeroDashboard.vue'
import HeroList from './components/HeroList.vue';

const routes = [
    { path: '/heroList', component: HeroList },
    { path: '/', component: HeroDashborad},
    { path: '/:heroId', component: HeroDetail, props: true},
];

const router = new VueRouter({
    routes
});

Vue.use(VueRouter);

const vm = new Vue({
    el: '#root',
    router
});
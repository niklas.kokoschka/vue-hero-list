Vue.component(
    'hero-list',
    require('./components/HeroList.vue').default
);

Vue.component(
    'hero-detail',
    require('./components/HeroDetail.vue').default
);

Vue.component(
    'hero-dashboard',
    require('./components/HeroDashboard.vue').default
);

Vue.component(
    'hero-log',
    require('./components/HeroLog.vue').default
);

